/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	
2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
	


	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.


	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.

*/


// Additional area
	function AddNumber(x,y) {
		    
		    let Sum = x + y; // adding process area
			console.log('Displayed Sum of ' + x + ' and ' + y);
			console.log(Sum);
               
			}
			AddNumber(5,15);



// Difference area
			function diffNumber(x,y) {
		    
		    let  diff = x - y; // difference process area
			console.log('Displayed difference of ' + x + ' and ' + y);
			console.log(diff);
               
			}
	     	diffNumber(20,5);



// Multiplication area
function multiNumber(x,y) {
		    
		    let  multi = x * y; // Multiplication process area
			console.log('The Product of ' + x + ' and ' + y);
			console.log(multi);
               
		}
		multiNumber(20,5);


//Quotient area
function quotNumber(x,y) {
		    
		    let  quot = x / y; // Divided process area
			console.log('The Quotient of ' + x + ' and ' + y);
			console.log(quot);
               
			}
			quotNumber(50,10);


// raduis area 
	function raduisNumber(Raduis) {

		let areaCirlce = Math.PI * Raduis * Raduis; // this is to get the area circle with 15 raduis

		console.log("The result of getting the area of a circle with: " + Raduis + " raduis");
		console.log(areaCirlce);

		};
		raduisNumber(15);


// Average Area 
	function AverageNum(num1, num2, num3, num4) {

     console.log("The Average ",+ num1 + ' '+ num2+ ' '+ num3 + ' and '+ num4)

		let Aver = num1 + num2 + num3 + num4; // adding all number

		let ResultAver = Aver / 4; // this area is to get the average of all 4 input number
		return ResultAver;
	};
	let Averrage1 = AverageNum(20, 40, 60, 80);
	console.log(Averrage1);


// Passing Score Area 
	function PassingScore(passing){
        
		console.log('Is 38/50 a passing Score');

		let PassScore = passing === passing;
		return PassScore;
	}

	let PassScore1 = PassingScore(34/50);
	console.log(PassScore1);